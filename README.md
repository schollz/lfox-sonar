# lfsurvey

This is a extension of FIND, [the Framework for Internal Navigation and Discovery](https://github.com/schollz/find), that is somewhat based off [Lucious Fox's technology in *Batman Begins*](http://batman.wikia.com/wiki/Lucius_Fox_(Morgan_Freeman)) that is used to track cellphones.

The system is based off a network of Raspberry Pis which sniff the WiFi probe requests from cellphones and sends these parcels to a central server that compiles them sends them to the [FIND server](https://github.com/schollz/find) which then uses machine learning to classify the location based on the unique WiFi fingerprints.

# Requirements

- [ ] Raspberry Pi
- [ ] USB Wifi adapters that support "monitor mode" and [support Raspbian](http://elinux.org/RPi_USB_Wi-Fi_Adapters)
- [ ] Cheap WiFi adaptor or ethernet connection
- [ ] Repeat for each additional Raspberry Pi (the more the better)

# Setup

1. Install Raspbian-lite onto a Pi. Make sure to give it a unique hostname but use the same password and same username (`pi`) for each one! Then initialize the Raspberry Pi with the following script
```
wget https://gist.githubusercontent.com/schollz/c2a449485a1767ef109c3d8f16b0b9ec/raw/802ebf68ea149eba1650a5c2383214614f95332a/initialize.sh
sudo ./initialize.sh
```
Alternatively, you can do this using [my script for PiBakery]().

2. On another computer, with access to all the Raspberry Pis, run 
```
git clone git@bitbucket.org:schollz/lfox-sonar.git
cd lfox-sonar/tools/
python3 cluster.py track
```
to which you'll be asked for the information about your cluster. Choose any `group` that you want, but remember it, as you will need it to login to the FIND server. For the `lf address`, you can use the default (a public server) or set it to your own. See `lfox-sonar/server/README.md` for more information.

3. Startup the Pi cluster using `python3.py cluster start`. You can check the status with `python3 cluster.py status`

4. After the cluster is up in running, you need to do learning. Take a smart phone and identify its mac address, something like `AA:BB:CC:DD:EE:FF`. Take your phone to a location. Then, on a computer, use `python3 cluster.py -u AA:BB:CC:DD:EE:FF -l location` where `location` is where your phone is. When you are done, use `python3 cluster.py track`. *This is important!* This turns off learning, and otherwise you'll be mixing signals for locations.

5. Repeat #4 for a few locations.

6. Now you are all set to track! Just run `python3 cluster.py track` and goto seee your tracking at https://ml.internalpositioning.com.





